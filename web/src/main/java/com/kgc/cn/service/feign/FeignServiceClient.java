package com.kgc.cn.service.feign;

import com.hjl.cn.beans.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author jialin
 * @createTime 2020-10-14
 */
@FeignClient(name = "boot-service", fallback = FeignServiceClient.TestHystrix.class)
public interface FeignServiceClient {
    
    @GetMapping(value = "/supportFeign/helloFeign")
    String helloFeign();
    
    @Component
    public class TestHystrix implements FeignServiceClient{

        @Override
        public String helloFeign() {
            return "fallback_test";
        }

        @Override
        public String withParam(String name) throws Exception {
            return null;
        }

        @Override
        public String withBody(User user) throws Exception {
            return null;
        }
    }
    
    @PostMapping(value = "/supportFeign/withParam")
    String withParam(@RequestParam("name") String name) throws Exception;
    
    @PostMapping(value = "/supportFeign/withBody")
    String withBody(@RequestBody User user) throws Exception;
    
}
