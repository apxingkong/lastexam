package com.kgc.cn.controller;

import com.hjl.cn.beans.User;
import com.kgc.cn.service.feign.FeignServiceClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author jialin
 * @createTime 2020-10-14
 */
@Api(tags = "Feign接口调用")
@RestController
@RequestMapping(value = "/feign")
public class TestFeignController {

    @Autowired
    private FeignServiceClient feignServiceClient;
    
    //http://localhost:8082/feign/testFeign
    @ApiOperation(value = "无参")
    @GetMapping(value = "/testFeign")
    public String testFeign(){
        return feignServiceClient.helloFeign();
    }
    
    //http://localhost:8082/feign/withParam?name=123
    @ApiOperation(value = "一参")
    @PostMapping(value = "/withParam")
    public String withParam(@RequestParam("name") String name) throws Exception {
        return feignServiceClient.withParam(name);
    }
    
    //http://localhost:8082/feign/withBody
    @ApiOperation(value = "对象")
    @PostMapping(value = "/withBody")
    public String withBody(@RequestBody User user) throws Exception {
        return feignServiceClient.withBody(user);
    }
    
}
