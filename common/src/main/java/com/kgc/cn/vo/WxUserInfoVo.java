package com.kgc.cn.vo;

import lombok.Data;

/**
 * @author jialin
 * @createTime 2020-09-16
 */
@Data
public class WxUserInfoVo {
    private String imgUrl;
    private String nName;
    private String sex;
}
