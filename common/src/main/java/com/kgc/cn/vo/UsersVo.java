package com.kgc.cn.vo;

import java.io.Serializable;

/**
 * @author jialin
 * @createTime 2020-09-09
 */
public class UsersVo implements Serializable {
    private int id;
    private String name;
    private WxUserInfoVo wxUserInfoVo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "UsersVo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", wxUserInfoVo=" + wxUserInfoVo +
                '}';
    }
}
