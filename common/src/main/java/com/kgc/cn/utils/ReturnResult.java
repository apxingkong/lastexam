package com.kgc.cn.utils;

import lombok.Data;

/**
 * @author jialin
 */
@Data
public class ReturnResult<T> {
    private String msg;
    private int code;
    private T data;
}
