package com.kgc.cn.beans;

import lombok.Data;

/**
 * @author jialin
 * @createTime 2020-10-14
 */
@Data
public class User {
    private Integer id;
    private String name;
    private Integer age;
}
