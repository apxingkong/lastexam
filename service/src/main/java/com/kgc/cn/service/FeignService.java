package com.kgc.cn.service;

import com.hjl.cn.beans.User;
import org.springframework.web.bind.annotation.*;

/**
 * @author jialin
 * @createTime 2020-10-14
 */
@RestController
@RequestMapping(value = "/supportFeign")
public class FeignService {
    
    @GetMapping(value = "/helloFeign")
    private String helloFeign(){
        return "hello feign";
    }
    
    @PostMapping(value = "/withParam")
    private String withParam(@RequestParam("name") String name){
        return "hello feign name2: " + name;
    }
    
    @PostMapping(value = "/withBody")
    private String withBody(@RequestBody User user){
        return "hello feign name2: " + user.toString();
    }
    
    
}
