package com.kgc.cn.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author jialin
 * @createTime 2020-09-08
 */
@Configuration
@MapperScan(basePackages = "com.kgc.cn.mapper")
public class MybatisConfig {
}
